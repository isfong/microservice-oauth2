package com.example.resource.server.resource;

import com.example.resource.server.domain.Train;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.Arrays.asList;

@RestController
@RequestMapping( "/trains" )
public class TrainResource {

    @GetMapping
    public ResponseEntity< List< Train > > getTrains( ) {
        return ResponseEntity.ok( asList( Train.builder( ).id( 1L ).express( true ).numberOfStats( 100 ).build( ),
                Train.builder( ).id( 2 ).express( false ).numberOfStats( 99 ).build( ),
                Train.builder( ).id( 3 ).express( true ).numberOfStats( 88 ).build( ) ) );
    }
}
