package com.example.resource.server.domain;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Builder
@Accessors( chain = true )
public class Train implements Serializable {
    private long id;
    private boolean express;
    private int numberOfStats;
}
