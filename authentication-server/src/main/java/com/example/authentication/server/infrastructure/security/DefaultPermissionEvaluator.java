package com.example.authentication.server.infrastructure.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

@Slf4j
public class DefaultPermissionEvaluator implements PermissionEvaluator {
    @Override
    public boolean hasPermission( Authentication authentication, Object targetDomainObject, Object permission ) {
        if ( authentication == null || targetDomainObject == null || !( permission instanceof String ) ) {
            return false;
        }
        String targetType = targetDomainObject.getClass( ).getSimpleName( ).toUpperCase( );
        return hasPrivilege( authentication, targetType, permission.toString( ).toUpperCase( ) );
    }

    @Override
    public boolean hasPermission( Authentication authentication, Serializable targetId, String targetType, Object permission ) {
        if ( authentication == null || targetType == null || !( permission instanceof String ) ) {
            return false;
        }
        return hasPrivilege( authentication, targetType, permission.toString( ).toUpperCase( ) );
    }

    private boolean hasPrivilege( Authentication authentication, String targetType, String permission ) {
        for ( GrantedAuthority grantedAuthority : authentication.getAuthorities( ) ) {
            log.info( "{} Contains granted authority {}", authentication.getPrincipal( ), grantedAuthority );
            String authority = grantedAuthority.getAuthority( );
            if ( authority.startsWith( targetType ) && authority.contains( permission ) ) {
                return true;
            }
        }
        return false;
    }
}
