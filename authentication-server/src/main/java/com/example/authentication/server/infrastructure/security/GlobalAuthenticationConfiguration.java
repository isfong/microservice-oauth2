package com.example.authentication.server.infrastructure.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@RequiredArgsConstructor
public class GlobalAuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter {
    private final UserDetailsService userDetailsService;

    @Override
    public void init( AuthenticationManagerBuilder auth ) throws Exception {
        auth.userDetailsService( userDetailsService );
    }

    @Bean
    public PasswordEncoder passwordEncoder( ) {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder( );
    }
}
