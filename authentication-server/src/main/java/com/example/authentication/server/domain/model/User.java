package com.example.authentication.server.domain.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
public class User {
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( nullable = false, unique = true )
    private String username;

    private String password;

    @ManyToMany( fetch = FetchType.EAGER )
    @JoinTable( name = Entities.USER_PRIVILEGES,
            joinColumns = { @JoinColumn( name = Entities.USER_ID, referencedColumnName = Entities.ID ) },//
            inverseJoinColumns = { @JoinColumn( name = Entities.PRIVILEGE_ID, referencedColumnName = Entities.ID ) } )
    private Set< Privilege > privileges;

    @ManyToOne( fetch = FetchType.EAGER )
    @JoinColumn( name = Entities.ORGANIZATION_ID, referencedColumnName = Entities.ID )
    private Organization organization;

    public User( String username, String password, Set< Privilege > privileges, Organization organization ) {
        this.username = username;
        this.password = password;
        this.privileges = privileges;
        this.organization = organization;
    }
}
