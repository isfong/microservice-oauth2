package com.example.authentication.server.domain.model;

import lombok.Data;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import org.codehaus.jackson.map.ObjectMapper;

import javax.persistence.*;

@Data
@Entity
@Accessors( chain = true )
public class Foo {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;
    @Column( nullable = false )
    private String name;

    @SneakyThrows
    @Override
    public String toString( ) {
        return "Foo" + new ObjectMapper( ).writeValueAsString( this );
    }
}
