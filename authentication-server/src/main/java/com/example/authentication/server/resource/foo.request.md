```shell script
curl http://localhost:8080/oauth/token  -u 'ci:cs' -d 'grant_type=password&username=john&password=123'
curl http://localhost:8080/foo/1 -H 'Authorization: Bearer [access_token]'
curl http://localhost:8080/foo -H 'Content-Type: application/json' -H 'Authorization: Bearer [access_token]' -d '{"id": null, "name": "NewFoo"}'
curl http://localhost:8080/organizations/2 -H 'Authorization: Bearer [access_token]'
```