package com.example.authentication.server.infrastructure.security;

import com.example.authentication.server.domain.model.User;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;


@Data
public class Principal implements UserDetails, java.security.Principal, Serializable {
    private static final long serialVersionUID = 1L;

    private User user;

    public Principal( User user ) {
        this.user = user;
    }

    @Override
    public String getName( ) {
        return user.getUsername( );
    }

    @Override
    public Collection< ? extends GrantedAuthority > getAuthorities( ) {
        return user.getPrivileges( ).stream( )
                .map( privilege -> new SimpleGrantedAuthority( privilege.getName( ) ) )
                .collect( Collectors.toList( ) );
    }

    @Override
    public String getPassword( ) {
        return user.getPassword( );
    }

    @Override
    public String getUsername( ) {
        return user.getUsername( );
    }

    @Override
    public boolean isAccountNonExpired( ) {
        return true;
    }

    @Override
    public boolean isAccountNonLocked( ) {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired( ) {
        return true;
    }

    @Override
    public boolean isEnabled( ) {
        return true;
    }
}
