package com.example.authentication.server.infrastructure.security;

import com.example.authentication.server.domain.model.User;
import org.springframework.security.access.expression.SecurityExpressionRoot;
import org.springframework.security.access.expression.method.MethodSecurityExpressionOperations;
import org.springframework.security.core.Authentication;

public class TypeOfMemberMethodSecurityExpressionRoot extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {

    private Object filterObject;
    private Object returnObject;

    /**
     * Creates a new instance
     *
     * @param authentication the {@link Authentication} to use. Cannot be null.
     */
    public TypeOfMemberMethodSecurityExpressionRoot( Authentication authentication ) {
        super( authentication );
    }

    /**
     * Definition isMember method security expression
     * used to check if current user is a member in given Organization.
     *
     * @param organizationId target Organization ID
     * @return Assert visitor does it belong to #organizationId member (true/false)
     */
    public boolean isMember( Long organizationId ) {
        Object principal = this.getPrincipal( );
        if ( !( principal instanceof Principal ) ) {
            return false;
        }
        User user = ( ( Principal ) principal ).getUser( );
        return user.getOrganization( ).getId( ).longValue( ) == organizationId.longValue( );
    }

    @Override
    public void setFilterObject( Object filterObject ) {
        this.filterObject = filterObject;
    }

    @Override
    public Object getFilterObject( ) {
        return this.filterObject;
    }

    @Override
    public void setReturnObject( Object returnObject ) {
        this.returnObject = returnObject;
    }

    @Override
    public Object getReturnObject( ) {
        return this.returnObject;
    }

    @Override
    public Object getThis( ) {
        return this;
    }
}
