package com.example.authentication.server.infrastructure.persistence;

import com.example.authentication.server.domain.model.Organization;
import com.example.authentication.server.domain.model.Privilege;
import com.example.authentication.server.domain.model.User;
import com.example.authentication.server.infrastructure.persistence.jpa.OrganizationRepository;
import com.example.authentication.server.infrastructure.persistence.jpa.PrivilegeRepository;
import com.example.authentication.server.infrastructure.persistence.jpa.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//@Component
@RequiredArgsConstructor
public class SetupData {
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final PrivilegeRepository privilegeRepository;
    private final OrganizationRepository organizationRepository;

    @PostConstruct
    public void init( ) {
        initPrivileges( );
        initOrganizations( );
        initUsers( );
    }

    private void initPrivileges( ) {
        this.privilegeRepository.save( new Privilege( "FOO_READ_PRIVILEGE" ) );
        this.privilegeRepository.save( new Privilege( "FOO_WRITE_PRIVILEGE" ) );
    }

    private void initOrganizations( ) {
        this.organizationRepository.save( new Organization( "FirstOrg" ) );
        this.organizationRepository.save( new Organization( "SecondOrg" ) );
    }

    private void initUsers( ) {
        Privilege read = this.privilegeRepository.findByName( "FOO_READ_PRIVILEGE" );
        Privilege write = this.privilegeRepository.findByName( "FOO_WRITE_PRIVILEGE" );
        Organization firstOrg = this.organizationRepository.findByName( "FirstOrg" );
        Organization secondOrg = this.organizationRepository.findByName( "SecondOrg" );

        this.userRepository.save( new User( "john", passwordEncoder.encode( "123" ), Stream.of( read ).collect( Collectors.toSet( ) ), firstOrg ) );
        this.userRepository.save( new User( "tom", passwordEncoder.encode( "111" ), Stream.of( write, read ).collect( Collectors.toSet( ) ), secondOrg ) );
    }
}
