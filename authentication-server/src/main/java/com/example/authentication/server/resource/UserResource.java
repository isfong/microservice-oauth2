package com.example.authentication.server.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;


@RestController
@RequestMapping( "/users" )
public class UserResource {

    @GetMapping
    public ResponseEntity< Principal > getUser( Principal user ) {
        return ResponseEntity.ok( user );
    }
}
