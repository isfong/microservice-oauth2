package com.example.authentication.server.infrastructure.security;

import org.springframework.context.annotation.Bean;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@EnableGlobalMethodSecurity( prePostEnabled = true )
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public PermissionEvaluator permissionEvaluator( ) {
        return new DefaultPermissionEvaluator( );
    }

//    @Bean
//    public ResponseEntityExceptionHandler responseEntityExceptionHandler( ) {
//        return new DefaultResponseEntityExceptionHandler( );
//    }


    @Override
    protected void configure( HttpSecurity http ) throws Exception {
        super.configure( http );
        // 请注意，antMatchers（）元素的顺序很重要-首先需要更具体的规则，然后是更通用的规则。
//        http
//                .csrf( ).disable( )
//                .authorizeRequests( )
//                .antMatchers( "/admin/**" ).hasRole( "ADMIN" )
//                .antMatchers( "/anonymous*" ).anonymous( )
//                .antMatchers( "/login*" ).permitAll( )
//                .anyRequest( ).authenticated( );

    }
}
