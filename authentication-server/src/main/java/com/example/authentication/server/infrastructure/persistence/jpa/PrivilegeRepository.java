package com.example.authentication.server.infrastructure.persistence.jpa;

import com.example.authentication.server.domain.model.Privilege;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrivilegeRepository extends CrudRepository< Privilege, Long > {
    Privilege findByName( String name );
}
