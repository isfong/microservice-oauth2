package com.example.authentication.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;


@EnableResourceServer
@SpringBootApplication
@EnableAuthorizationServer
public class AuthenticationServerApplication {

    public static void main( String[] args ) {
        SpringApplication.run( AuthenticationServerApplication.class, args );
    }

}
