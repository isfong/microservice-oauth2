package com.example.authentication.server.domain.model;

public interface Entities {
    String ID = "id";
    String USER_ID = "user_id";
    String PRIVILEGE_ID = "privilege_id";
    String ORGANIZATION_ID = "organization_id";
    String USER_PRIVILEGES = "user_privileges";
}
