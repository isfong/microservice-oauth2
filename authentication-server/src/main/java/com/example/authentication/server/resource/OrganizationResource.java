package com.example.authentication.server.resource;

import com.example.authentication.server.domain.model.Organization;
import com.example.authentication.server.infrastructure.persistence.jpa.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping( "/organizations" )
public class OrganizationResource {

    private final OrganizationRepository organizationRepository;

    @GetMapping( "/{id}" )
    @PreFilter( "#principal.organizationId == #id" )
    public ResponseEntity< Organization > getOrganization( @PathVariable final long id ) {
        return this.organizationRepository
                .findById( id )
                .map( ResponseEntity::ok )
                .orElse( ResponseEntity.notFound( ).build( ) );
    }
}
