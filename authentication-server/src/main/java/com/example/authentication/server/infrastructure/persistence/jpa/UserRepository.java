package com.example.authentication.server.infrastructure.persistence.jpa;

import com.example.authentication.server.domain.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository< User, Long > {
    Optional< User > findByUsername( String username );
}
