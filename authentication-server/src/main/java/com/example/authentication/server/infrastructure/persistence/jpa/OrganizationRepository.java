package com.example.authentication.server.infrastructure.persistence.jpa;

import com.example.authentication.server.domain.model.Organization;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrganizationRepository extends CrudRepository< Organization, Long > {
    Organization findByName( String name );
}
