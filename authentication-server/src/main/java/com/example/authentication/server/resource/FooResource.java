package com.example.authentication.server.resource;

import com.example.authentication.server.domain.model.Foo;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.Random;

@RestController
@RequestMapping( "/foo" )
public class FooResource {

    @GetMapping( "/{id}" )
    @PostAuthorize( "hasPermission(returnObject.body, 'read')" )
    public ResponseEntity< Foo > getFoo( @PathVariable long id ) {
        return ResponseEntity.ok( new Foo( ).setId( id ).setName( "Simple" ) );
    }

    @PostMapping
    @PreAuthorize( "hasPermission(#foo, 'write')" )
    public ResponseEntity< Foo > createFoo( @RequestBody Foo foo ) {
        foo.setId( new Random( ).nextLong( ) );
        return ResponseEntity
                .created( ServletUriComponentsBuilder//
                        .fromCurrentRequest( )
                        .path( "/{id}" )
                        .build( foo.getId( ) ) )
                .body( foo );
    }
}
